import numpy as np


def get_pixel(img):
    h, w = img.shape[0], img.shape[1]

    pixels = []
    for i in range(h):
        for j in range(w):
            pixels.append(img[i, j])

    return np.array(pixels)


class GMM:

    def __init__(self, num_clusters=3, data_dim=3, thres=1e-5, num_steps=20):
        self.num_clusters = num_clusters
        self.data_dim = data_dim
        self.thres = thres
        self.num_steps = num_steps

        self.mu = np.zeros(shape=(self.num_clusters, self.data_dim))
        self.sigma = np.zeros(shape=(self.num_clusters, self.data_dim, self.data_dim))
        self.weight = np.zeros(shape=self.num_clusters)


    def cal_prob(self, x, mu, sigma, det, inv):
        d = len(x)

        scale = 1 / (((2 * np.pi) ** (d / 2)) * np.sqrt(np.absolute(det)) + 1e-18)

        x_mu = x - mu

        prob = np.dot(np.dot(x_mu.T, inv), x_mu)

        value2 = scale * np.exp(-0.5 * prob)

        return value2


    def train(self, img):
        pixels = get_pixel(img)
        num_pixels, d = pixels.shape[0], pixels.shape[1]

        # Initialization

        random_index_mu = np.random.choice(num_pixels, self.num_clusters)
        self.mu = pixels[random_index_mu, :]

        self.sigma = [np.eye(d) * 100] * self.num_clusters
        self.weight = [1 / self.num_clusters for i in range(self.num_clusters)]

        self.det = np.zeros(shape=(self.num_clusters))
        for k in range(self.num_clusters):
            self.det[k] = np.linalg.det(self.sigma[k])

        self.inv_sigma = np.zeros(shape=(self.num_clusters, self.data_dim, self.data_dim))
        for k in range(self.num_clusters):
            self.inv_sigma[k] = np.linalg.inv(self.sigma[k])

        R = np.zeros((num_pixels, self.num_clusters))

        log_likelihood = -1
        firstTime = True

        for id_step in range(self.num_steps):

            # E-step:
            for k in range(self.num_clusters):
                for i in range(num_pixels):
                    value = self.cal_prob(pixels[i], self.mu[k], self.sigma[k], self.det[k], self.inv_sigma[k])
                    R[i, k] = value

            previous_log_likelihood = log_likelihood
            log_likelihood = np.sum(np.log(np.sum(R, axis=1)))

            print('Step: %d, log_likelihood: %.2lf' % (id_step, log_likelihood))

            R = (R.T / (np.sum(R, axis=1) + 1e-18)).T
            sum_k = np.sum(R, axis=0)

            # M-step:
            for k in range(self.num_clusters):
                self.mu[k] = 1.0 / sum_k[k] * np.sum(R[:, k] * pixels.T, axis=1).T
                x_mu = np.matrix(pixels - self.mu[k])
                self.sigma[k] = np.array(1 / sum_k[k] * np.dot(np.multiply(x_mu.T, R[:, k]), x_mu))
                self.weight[k] = 1 / num_pixels * sum_k[k]

            # Update det
            for k in range(self.num_clusters):
                self.det[k] = np.linalg.det(self.sigma[k])

            for k in range(self.num_clusters):
                self.inv_sigma[k] = np.linalg.pinv(self.sigma[k])

            if firstTime == False and np.abs(previous_log_likelihood - log_likelihood) < self.thres:
                break

            firstTime = False

    def assign_cluster(self, pixel):

        probs = []
        for i in range(self.num_clusters):
            probs.append(self.cal_prob(pixel, self.mu[i], self.sigma[i], self.det[i], self.inv_sigma[i]))

        return np.argmax(np.array(probs))



