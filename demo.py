from PIL import Image
import numpy as np
from gmm import GMM
import matplotlib.pyplot as plt
import cv2


IMAGE_NAME = 'rainbow.png'

if __name__ == "__main__":
    # Read image from disk
    img = np.array(Image.open(IMAGE_NAME).resize((256, 256)))

    # img = np.int32(img)
    # noise = np.random.normal(0, 5, img.shape)
    #
    # img += np.uint8(noise)


    # Initialize Gaussian Mixture Model with its paramters
    gmm = GMM(num_clusters=5, num_steps=100)

    # Train GMM
    gmm.train(img)

    # Assign pixels in image with trained GMM
    img_cluster = np.zeros(shape=(img.shape[0], img.shape[1]))
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            img_cluster[i, j] = gmm.assign_cluster(img[i, j])

    # Visualize results
    plt.imshow(img)
    plt.show()

    img_cluster = img_cluster * 255 // np.max(img_cluster)

    img_cluster = np.uint8(img_cluster)
    img_cluster = cv2.applyColorMap(img_cluster, cv2.COLORMAP_JET)

    plt.imshow(img_cluster)
    plt.show()


